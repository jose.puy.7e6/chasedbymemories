# ChasedByMemories



## Description

Chased By Memories es un juego de accion con toques Rogue Like, con insiparción en juegos como Enter The Gungeon o The Binding of Isaac. Tiene un estilo caracteristico pixel, con vista TopDown.

## Instructions

- [ ]  Utiliza las flechas o las letras WASD para moverte ![Teclas de movimiento](https://as1.ftcdn.net/v2/jpg/04/67/50/50/1000_F_467505041_P4vMqB8Ungt2mXC3yv9NzueFhKZzI4YL.jpg )

- [ ] Apunta con el ratón y haz click para disparar.

- [ ] Encuentra la salida evitando a los enemigos, y curate usando los corazones.
