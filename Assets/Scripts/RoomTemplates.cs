using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTemplates : MonoBehaviour
{
    public GameObject[] bottomRooms;
    public GameObject[] topRooms;
    public GameObject[] leftRooms;
    public GameObject[] rightRooms;

    public int roomNumber = 0;

    public GameObject[] initialRooms;
    public GameObject[] endRooms;

    private int rnd;

    public bool ending;

    //private void Start()
    //{
    //    rnd = Random.Range(0, initialRooms.Length);
    //    Instantiate(initialRooms[rnd]);

    //}


    public GameObject[] walls;
}
