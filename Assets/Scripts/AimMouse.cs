using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimMouse : MonoBehaviour
{
    public Camera mainCamera;
    public Vector3 mousePosition;

    public SpriteRenderer spriteRenderer;

    PlayerMovement playerMovement;

    private void Start()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
    }

    private void Update()
    {
        mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);

        Vector3 rotation = mousePosition - transform.position;

        float rotZ = Mathf.Atan2(rotation.y, rotation.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.Euler(0, 0, rotZ);

        if (playerMovement.hasWeapon == true)
        {
            if (rotZ < 89 && rotZ > -89)
            {
                GameObject.Find("Weapon").GetComponent<SpriteRenderer>().flipY = false;
            }
            else
            {
                GameObject.Find("Weapon").GetComponent<SpriteRenderer>().flipY = true;
            }
        }

    }
}
