using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] public AudioSource pickUp;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Weapon"))
        {
            Debug.Log("Coge arma");
            GetComponent<PlayerMovement>().hasWeapon = true;
            pickUp.Play();
            Destroy(collision);
        }
    }
}
