using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomGenerator : MonoBehaviour
{
    public GameObject[] startRooms;
    public GameObject[] mainRooms;
    public GameObject[] endRooms;
}
