using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD : MonoBehaviour
{
    public GameObject[] vidas;

    public void DesactivarVida(int i)
    {
        vidas[i].SetActive(false);
    }
    
    public void ActivarVida(int i)
    {
        vidas[i].SetActive(true);
    }
}
