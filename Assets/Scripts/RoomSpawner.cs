using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

public class RoomSpawner : MonoBehaviour
{

    public int openSide;

    //1 Need Bottom door
    //2 Need Top door
    //3 Need Left door
    //4 Need Right door

    private RoomTemplates templates;
    private int rand;
    private bool spawned = false;


    void Start()
    {
        templates = GameObject.FindGameObjectWithTag("Rooms").GetComponent<RoomTemplates>();
        Invoke(nameof(Spawn), 0.1f);
    }

    public void Spawn()
    {
        if (spawned == false && templates.roomNumber <= 15)
        {


            switch (openSide)
            {
                case 1:
                    //Debug.Log("Spawn BOT");
                    Instanciar(templates.bottomRooms);
                    break;

                case 2:
                    //Debug.Log("Spawn TOP");
                    Instanciar(templates.topRooms);
                    break;

                case 3:
                    //Debug.Log("Spawn LEFT");
                    Instanciar(templates.leftRooms);
                    break;

                case 4:
                    //Debug.Log("Spawn RIGHT");
                    Instanciar(templates.rightRooms);
                    break;
            }

            spawned = true;

        }else
        {
            switch (openSide)
            {
                case 1:
                    //Debug.Log("Spawn BOT");
                    if (templates.ending == false)
                    {
                        Instantiate(templates.endRooms[2], transform.position, templates.endRooms[2].transform.rotation);
                    }
                    else
                    {
                        Instantiate(templates.bottomRooms[0], transform.position, templates.bottomRooms[0].transform.rotation);
                    }
                    break;

                case 2:
                    //Debug.Log("Spawn TOP");
                    if (templates.ending == false)
                    {
                        Instantiate(templates.endRooms[0], transform.position, templates.endRooms[0].transform.rotation);
                    }
                    else
                    {
                        Instantiate(templates.topRooms[0], transform.position, templates.topRooms[0].transform.rotation);
                    }
                    break;

                case 3:
                    //Debug.Log("Spawn LEFT");
                    if (templates.ending == false)
                    {
                        Instantiate(templates.endRooms[3], transform.position, templates.endRooms[3].transform.rotation);
                    }
                    else
                    {
                        Instantiate(templates.leftRooms[0], transform.position, templates.leftRooms[0].transform.rotation);
                    }
                    break;

                case 4:
                    //Debug.Log("Spawn RIGHT");
                    if (templates.ending == false)
                    {
                        Instantiate(templates.endRooms[1], transform.position, templates.endRooms[1].transform.rotation);
                    }
                    else
                    {
                        Instantiate(templates.rightRooms[0], transform.position, templates.rightRooms[0].transform.rotation);
                    }
                    break;
            }
            templates.ending = true;
            spawned = true;
        }
    }

    private void Instanciar(GameObject[] template)
    {
        rand = Random.Range(1, template.Length);
        Instantiate(template[rand], transform.position, template[rand].transform.rotation);
        templates.roomNumber++;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("SpawnPoint"))
        {
            Debug.Log("Destruido");
            Destroy(gameObject);
            spawned = true;
        }
    }
}
