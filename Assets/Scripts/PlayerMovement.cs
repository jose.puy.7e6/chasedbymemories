using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float speed;

    private Rigidbody2D playerRb;
    private Vector2 moveInput;

    public Camera mainCamera;
    public Vector3 mousePosition;

    private Animator playerAnimator;

    public GameManager gameManager;

    private Inventory playerInventory;

    public bool isShooting;

    public bool hasWeapon;


    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();

        mainCamera = Camera.main;

        //gameManager = GetComponent<GameManager>();

        playerInventory = GetComponent<Inventory>();

    }

    // Update is called once per frame
    void Update()
    {
        HandleInventory();
        HandleShooting();

        mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        Vector3 direction = mousePosition - transform.position;

        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");
        moveInput = new Vector2(moveX, moveY).normalized;
        //moveInput = direction;

        playerAnimator.SetFloat("Horizontal", direction.x);
        playerAnimator.SetFloat("Vertical", direction.y);
        playerAnimator.SetFloat("Speed", moveInput.sqrMagnitude);

        if (gameManager.vidas == 0)
        {
            Debug.Log("vidas 0");
            playerAnimator.SetBool("Dead", true);
        }
    }

    private void HandleShooting()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isShooting = true;
        }

        if (Input.GetMouseButtonUp(0)) 
        {
            isShooting = false;
        }
    }

    private void HandleInventory()
    {
        var keyboard = Keyboard.current;


        if (keyboard.digit1Key.isPressed)
        {
            playerInventory.ChangeSlot(1);
        }
        if (keyboard.digit2Key.isPressed)
        {
            playerInventory.ChangeSlot(2);
        }
        //if (keyboard.gKey.isPressed)
        //{
        //    playerInventory.DropItem();
        //}


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Finish"))
        {
            gameManager.EndGame();
        }
    }

    private void FixedUpdate()
    {
        playerRb.MovePosition(playerRb.position + moveInput * speed * Time.fixedDeltaTime);
    }
}
