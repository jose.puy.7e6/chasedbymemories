using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    [SerializeField] ItemData weapon;

    public GameObject item;

    public GameManager gameManager;

    public GameObject instance;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && gameManager.monedas == 100 )
        {
            gameManager.monedas = 0;
            PickUp();
            Destroy(gameObject);
        }
    }

    void PickUp()
    {
        var newObj = Instantiate(instance, item.gameObject.transform);
        newObj.name = "Weapon";
        //item.GetComponent<SpriteRenderer>().sprite = weapon.icon;
    }
}
