using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class BaseWeapon : MonoBehaviour
{
    [SerializeField]
    Transform muzzle;

    float nextTimeToFire = 0f;

    [SerializeField]
    protected ItemData mItemData;

    PlayerMovement playerMovement;

    public void Start()
    {
        playerMovement = transform.root.GetComponent<PlayerMovement>();
    }

    bool CheckFireRate()
    {
        if(Time.time > nextTimeToFire)
        {
            nextTimeToFire = Time.time + (1f / mItemData.fireRate);
            return true;
        }
        return false;
    }


    public void Update()
    {
        if (playerMovement.isShooting)
        {
            if (mItemData.shotType == ShotType.Auto)
            {
                InstanceBullet(muzzle);
                //if (CheckFireRate())
                //{
                //    InstanceBullet(muzzle);
                //}
            }
            else if (mItemData.shotType == ShotType.Single)
            {
                InstanceBullet(muzzle);
                playerMovement.isShooting = false;
            }
        }
    }

    public GameObject InstanceBullet(Transform origin)
    {
        GameObject projectile = Instantiate(
            mItemData.bulletPrefab,
            origin.position,
            origin.rotation,
            null
        );

        return projectile;
    }

    public abstract void Shoot();
}
