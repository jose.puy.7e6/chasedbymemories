using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameThrower : BaseWeapon
{

    public GameObject[] muzzles;

    private PlayerMovement mMovement;

    [SerializeField] public AudioSource effect;

    private bool sound;


    public override void Shoot()
    {
        throw new System.NotImplementedException();
    }

    // Start is called before the first frame update
    void Start()
    {
        base.Start(); 

        mMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
    }



    // Update is called once per frame
    void Update()
    {
        base.Update();


        if ( mMovement.isShooting && sound == false)
        {
            effect.Play();
            sound = true;
        }else
        {
            sound = false;
        }
    }
}
