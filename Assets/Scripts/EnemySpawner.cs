using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public Door door;
    public GameObject[] spawns;
    public GameObject[] enemyList;

    public GameObject[] enemies;

    private bool spawned = false;

    private int rnd;

    private void Start()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && spawned == false)
        {
            door.SpawnDoors();
            foreach(GameObject spawner in spawns)
            {
                Spawn(spawner);
            }
            
            Debug.Log("Player on Room");
            spawned = true;
        }
    }

    private void Update()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemies");

        if (enemies.Length == 0 && spawned)
        {
            door.Deactivate();
        }
    }


    void Spawn(GameObject item)
    {
        rnd = UnityEngine.Random.Range(0, enemyList.Length);
        Instantiate(enemyList[rnd], item.transform.position, Quaternion.identity);
    }
}
