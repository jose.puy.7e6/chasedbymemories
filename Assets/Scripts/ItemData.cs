using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Item/Create New Item")]
public class ItemData : ScriptableObject
{
    [SerializeField] public string itemName;
    [TextArea]
    [SerializeField] public string itemDesc;
    [SerializeField] public Sprite icon;
    [SerializeField] public int itemCost;
    [SerializeField] public int damage;
    [SerializeField] public ShotType shotType;
    [SerializeField] public float fireRate;
    [SerializeField] public GameObject bulletPrefab;
    [SerializeField] public BaseWeapon baseWeapon;

    void Init()
    {

    }
    
}

public enum ShotType
{
    Auto,
    Single
}