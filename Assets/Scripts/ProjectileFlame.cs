using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileFlame : MonoBehaviour
{
    public float speed;

    private Transform player;
    private Vector2 target;

    public Camera mainCamera;

    public bool instanciated;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;

        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();


        target = mainCamera.ScreenToWorldPoint(Input.mousePosition);
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);

        Wait();
        DestroyProjectile();
        
    }


    public void DestroyProjectile()
    {
        Destroy(gameObject);
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(3);
        Debug.Log("he waiteado fuego");
    }
}
