using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public bool[] isFull;
    public Slot[] slots;
    public int equippedSlot;

    private GameObject equippedBorder;

    private void Start()
    {
        equippedBorder = GameObject.FindGameObjectWithTag("SelectedBorder");
        equippedSlot = 0;
        //slots[0] = GameObject.FindFirstObjectByType<Slot>();
        slots = GameObject.FindObjectsOfType<Slot>();
    }

    private void Update()
    {
        switch (equippedSlot)
        {
            case 0:
                equippedBorder.transform.localPosition = new Vector3(-50, -189, 0);
                break;
            case 1:
                equippedBorder.transform.localPosition = new Vector3(50, -189, 0);
                break;
        }
    }

    public void ChangeSlot(int pressed)
    {
        switch (pressed)
        {
            case 1:
                this.equippedSlot = 0;
                break;
            case 2:
                this.equippedSlot = 1;
                break;
        }
    }

    public void DropItem()
    {
        slots[equippedSlot].DropItem();

        //Debug.Log(slots[equippedSlot].gameObject.name);
        ////Component slot = slots[equippedSlot].gameObject;

        //switch (equippedSlot)
        //{
        //    case 0:
        //        slots[0].DropItem();
        //        break;
        //    case 1:
        //        //slot2.DropItem();
        //        break;
        //};
    }
}
