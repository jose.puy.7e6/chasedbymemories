using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Enemy2 : MonoBehaviour
{
    public float speed;

    private Transform player;
    private Vector2 target;

    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;

        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            anim.SetBool("Dead", true);
            speed = 0;
            StartCoroutine("Wait");
            Debug.Log("Destruido");
        }
    }

    public void DestroyEnemy()
    {
        Destroy(gameObject);
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(1);
        Debug.Log("he waiteado");
        DestroyEnemy();
    }
}
