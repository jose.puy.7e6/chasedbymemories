using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public HUD hud;

    private Animator anim;

    public int vidas = 3;

    public int monedas = 100;

    public GameObject coins;

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.Log("Mas de un game manager");
        }
    }

    public void Update()
    {
        coins.GetComponent<Text>().text = monedas.ToString();

        if (vidas == 0)
        {
            SceneManager.LoadScene(2);
        }
    }

    public void PerderVida()
    {
        vidas -= 1;
        hud.DesactivarVida(vidas);
    }

    public bool RecuperarVida()
    {
        if (vidas == 3)
        {
            Debug.Log("Max vida");
            return false;
        }

        hud.ActivarVida(vidas);
        vidas += 1;
        return true;
    }

    public void EndGame()
    {
        SceneManager.LoadScene(3);
    }
}
